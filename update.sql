# The syntax of this file is built for PostgreSQL

#######################################################
#
# ANNOUNCEMENT BANNER
#
# Update the contents that you want to appear in your 
# announcement banner and whether it should be public
# or private
#
#######################################################

update propertytext
        set propertyvalue = 'THIS IS A TEST SYSTEM'
        where propertytext.id = (
        	select id
        		from "propertyentry"
        		where entity_name = 'jira.properties' and entity_id = '1' and property_key = 'jira.alertheader');


update propertystring
        set propertyvalue = 'public'
        where propertystring.id = (
	        select id
		        from "propertyentry"
		        where entity_name = 'jira.properties' and entity_id = '1' and property_key = 'jira.alertheader.visibility');


#######################################################
#
# BASE URL
#
# Update the Base URL 
#
#######################################################


update propertystring
	set propertyvalue = 'http://jira.servername.com'
from propertyentry as PE
	where PE.id=propertystring.id
	and PE.property_key = 'jira.baseurl';



#######################################################
#
# User Directory
#
# Update the User Directory Configuration
# Important ones are basedb, url, username, password, 
# interval, and id. Remember to replicate this as many times as
# directories tyou need to update.
#
# if you need to update anything else, pull it from the
# cwd_directory_attribute table
#
#######################################################

update cwd_directory_attribute
	set attribute_value = 'o=sevenSeas'
	where directory_id = '10000'
	and attribute_name = 'ldap.basedn';

update cwd_directory_attribute
	set attribute_value = 'ldap://localhost:10389'
	where directory_id = '10000'
	and attribute_name = 'ldap.url';

update cwd_directory_attribute
	set attribute_value = 'uid=admin,ou=system'
	where directory_id = '10000'
	and attribute_name = 'ldap.userdn';

update cwd_directory_attribute
	set attribute_value = 'password'
	where directory_id = '10000'
	and attribute_name = 'ldap.password';

update cwd_directory_attribute
	set attribute_value = '3600'
	where directory_id = '10000'
	and attribute_name = 'directory.cache.synchronise.interval';


#######################################################
#
# Outgoing Mail Server
#
# 
#
#######################################################

# Disable standard outgoing mail
UPDATE propertynumber SET propertyvalue=1 WHERE ID=(SELECT ID FROM propertyentry WHERE ENTITY_NAME='jira.properties' AND ENTITY_ID=1 AND PROPERTY_KEY='jira.mail.send.disabled');

# Update outgoing mail prefix - set as appropriate to environment
UPDATE mailserver SET prefix='[JIRA ENVIRONMENT-GOES-HERE]' WHERE ID=(SELECT ID FROM mailserver WHERE protocol like 'smtps');

#######################################################
#
# Incoming Mail Server
#
# Update the host, username, port, and password
# smtp_port is the right value regardless of protocol, 
# its being overloaded
#
# If you want to update other properties, you can get
# them from the mailserver table.
#
#######################################################


UPDATE mailserver 
	SET SERVERNAME = 'example.com', mailusername = 'example', mailpassword = 'password', smtp_port = '110'
	WHERE ID = '10000';

# If you want to delete the inbound handlers and server instead

# Delete parent property entries
DELETE FROM propertyentry WHERE property_key in ('handler','handler.params','popserver');

# Remove the existing mail jobs
DELETE FROM clusteredjob WHERE JOB_ID IN (SELECT CONCAT('com.atlassian.jira.service.JiraService:', ID) FROM serviceconfig WHERE CLAZZ = 'com.atlassian.jira.service.services.mail.MailFetcherService');

# Delete mail service configuration
DELETE FROM serviceconfig WHERE CLAZZ = 'com.atlassian.jira.service.services.mail.MailFetcherService';


#######################################################
#
# JSD Mail Handlers
#
# You can update the server, and then make sure to
# Update the mail channel as well to point at the new
# email address.
#
#######################################################


UPDATE "AO_2C4E5C_MAILCONNECTION"
	SET "HOST_NAME" = 'imap.gmail.com', "PORT" = '993', "PROTOCOL" = 'imaps', "USER_NAME"  = 'USERNAME', "PASSWORD" = "PASSWORD", "EMAIL_ADDRESS"  = 'username@example.com', "FOLDER" = 'inbox', "TLS"  = 'f'
	WHERE "ID" = 1;


UPDATE "AO_54307E_EMAILCHANNELSETTING"
	SET "EMAIL_ADDRESS" = 'username@example.com'
	WHERE "ID" = 1;


# The alternative is to disable the JSD incoming mail handler
# UPDATE propertynumber pn SET propertyvalue = 0 WHERE ID = (SELECT ID FROM propertyentry WHERE property_key = 'GLOBAL_PULLER_JOB');


#######################################################
#
# Introduction Gadget Text
#
# Update the content of the Introduction Gadget
#
#######################################################


update propertytext
        set propertyvalue = 'System was refreshed at ' || timeofday() || ' Thanks for your cooperation.'
        where id = (
        	select id
        		from propertyentry
        		where entity_name = 'jira.properties' and entity_id = '1' and property_key = 'jira.introduction');


#######################################################
#
# Application Links
#
# Delete all Application links
#
# Note: There is something that this is still 
# overlooking which results in some harmless but spammy
# log messages 
#
# Note 2: This portion was only tested with MS SQL
#
#######################################################


DELETE FROM oauthspconsumer
DELETE FROM oauthconsumer;
DELETE FROM oauthconsumertoken 
DELETE FROM trustedapp;
DELETE FROM oauthsptoken;


DELETE FROM propertystring WHERE id IN (
	SELECT id FROM propertyentry 
	INNER JOIN (SELECT substring(a.property_key,16,36) as prop_key FROM propertyentry a join propertystring b on a.id=b.id where a.property_key like 'applinks.admin%name') L
	ON property_key LIKE 'applinks.%'+L.prop_key+'%'
);


DELETE p FROM propertyentry p
INNER JOIN (SELECT substring(a.property_key,16,36) as prop_key FROM propertyentry a join propertystring b on a.id=b.id where a.property_key like 'applinks.admin%name') L
ON property_key LIKE 'applinks.%'+L.prop_key+'%';

DELETE p FROM propertytext p WHERE id in (
	SELECT id FROM propertyentry 
	INNER JOIN (SELECT substring(a.property_key,16,36) as prop_key FROM propertyentry a join propertystring b on a.id=b.id where a.property_key like 'applinks.admin%name') L
	ON property_key LIKE '%ual.'+L.prop_key+'%');

DELETE p FROM propertyentry p
INNER JOIN (SELECT substring(a.property_key,16,36) as prop_key FROM propertyentry a join propertystring b on a.id=b.id where a.property_key like 'applinks.admin%name') L
ON property_key LIKE '%ual.'+L.prop_key+'%';

DELETE p FROM propertyentry p WHERE id in (
	SELECT id FROM propertystring
	INNER JOIN (SELECT substring(a.property_key,16,36) as prop_key FROM propertyentry a join propertystring b on a.id=b.id where a.property_key like 'applinks.admin%name') L
	ON propertyvalue LIKE '%'+L.prop_key+'%');

DELETE p FROM propertystring p
INNER JOIN (SELECT substring(a.property_key,16,36) as prop_key FROM propertyentry a join propertystring b on a.id=b.id where a.property_key like 'applinks.admin%name') L
ON propertyvalue LIKE '%'+L.prop_key+'%';

DELETE p FROM propertyentry p WHERE id in (
	SELECT id FROM propertytext 
	INNER JOIN (SELECT substring(a.property_key,16,36) as prop_key FROM propertyentry a join propertystring b on a.id=b.id where a.property_key like 'applinks.admin%name') L
	ON propertyvalue LIKE '%'+L.prop_key+'%');

DELETE p FROM propertytext p
INNER JOIN (SELECT substring(a.property_key,16,36) as prop_key FROM propertyentry a join propertystring b on a.id=b.id where a.property_key like 'applinks.admin%name') L
ON propertyvalue LIKE '%'+L.prop_key+'%';

DELETE p FROM propertystring p where id in (select id from propertyentry where property_key like 'applinks.global%');

DELETE from AO_21D670_WHITELIST_RULES where "TYPE" = 'APPLICATION_LINK';

Delete from propertyentry where PROPERTY_KEY like 'applinks.admin%';

# Don't remember how we got the ID in this one:
# UPDATE propertytext SET propertyvalue = '#java.util.List\n' WHERE id = '83329';


#############################
# 
# Update Jira Server ID
# Full docs at https://confluence.atlassian.com/jirakb/change-the-server-id-for-an-instance-of-jira-server-285839562.html
# 
############################


UPDATE propertystring SET propertyvalue = '<ID>' where id = (select id from propertystring where id in (select id from propertyentry where PROPERTY_KEY='jira.sid.key'));
delete from oauthconsumer where consumerservice = '_HOST_SERVICE_';


#############################
# 
# Data Center Cluster Sanitization
# 
############################


DELETE FROM clusternode;
DELETE FROM clusternodeheartbeat;
DELETE FROM replicatedindexoperation;


#############################
# 
# Deactivate all the webhooks so that changes to issues in staging don't call
# 
############################


update AO_4AEACD_WEBHOOK_DAO set ENABLED=0;


#############################
# 
# Look and Feel
# 
############################


# Set the color for the top bar
update propertystring set propertyvalue='#8d69db' where id=10500;
